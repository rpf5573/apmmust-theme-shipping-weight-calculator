<?php

class Select_Nav_Walker extends Walker_Nav_Menu
{
  function start_lvl(&$output, $depth = 0, $args = array())
  {
  }
  function end_lvl(&$output, $depth = 0, $args = array())
  {
  }
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
  {
    $cat_id = $item->object_id;

    $weight = rwmb_meta('weight', ['object_type' => 'term'], $cat_id) ?: 0;

    $output .= '<option ' . ($weight === 0 ? 'disabled' : '') . ' value="' . $weight . '">';
    $output .= str_repeat(" - ", $depth);

    $output .= $item->title;
  }
  function end_el(&$output, $item, $depth = 0, $args = array())
  {
    $output .= "</option>";
  }
}
